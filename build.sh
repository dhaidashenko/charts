mkdir public
cd public
for d in ../*/; do
  if [[ "$d" == "../public/" ]]; then
    continue
  fi
  helm package $d
done
helm repo index ./
