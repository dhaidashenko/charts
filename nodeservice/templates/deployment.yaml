{{- $fullName := include "service.fullname" . -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $fullName }}
  labels:
{{ include "service.labels" . | indent 4 }}
spec:
  {{- if ge (.Values.replicaCount|int) 0 }}
  replicas: {{ .Values.replicaCount }} # by default is 1
  {{- end }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ include "service.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
  {{- with .Values.strategy }}
  strategy:
    {{- . | toYaml | nindent 4 }}
  {{- end }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ include "service.name" . }}
        app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Values.istio.pod_inject }}
        sidecar.istio.io/inject: "true"
{{- end }}
      annotations:
        checksum/config: {{ toYaml .Values.configYaml | sha256sum | quote }}
    spec:
{{- if .Values.image.pullSecret }}
      imagePullSecrets:
        - name: {{ .Values.image.pullSecret }}
{{- end }}
      volumes:
        - name: config
          secret:
            secretName: {{ $fullName }}
{{- if .Values.gcp }}
        - name: google-cloud-key
          secret:
            secretName: {{ .Values.gcp.secretName}}
{{- end }}
{{- if .Values.migrations.enabled }}
      initContainers:
      - name: init
        image: "{{ .Values.image.repository }}:{{ .Values.image.version }}"
        imagePullPolicy: {{ .Values.image.pullPolicy }}
        args:
          - migrate
          - up
        volumeMounts:
          - name: config
            mountPath: "/config"
            readOnly: true
{{- end }}
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.version }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          args:
{{- range .Values.runtime.args }}
            - {{ . | quote }}
{{- end }}
          ports:
            - name: http
              containerPort: 80
            - name: grpc
              containerPort: 8080
            - name: metrics
              containerPort: 8000
    {{- with .Values.livenessProbe }}
          livenessProbe:
{{ toYaml . | indent 12 }}
    {{- end }}
    {{- with .Values.readinessProbe }}
          readinessProbe:
{{ toYaml . | indent 12 }}
    {{- end }}
          volumeMounts:
            - name: config
              mountPath: "/config"
              readOnly: true
  {{- if .Values.gcp }}
            - name: google-cloud-key
              mountPath: /var/secrets/google
  {{- end }}
          env:
{{- range $name, $value := .Values.environment }}
            - name: {{ $name | quote }}
              value: {{ $value | quote }}
{{- end }}
  {{- if .Values.gcp }}
            - name: GOOGLE_APPLICATION_CREDENTIALS
              value: /var/secrets/google/{{ .Values.gcp.keyFileName }}
  {{- end }}
          resources:
{{ toYaml .Values.resources | indent 12 }}
    {{- with .Values.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.affinity }}
      affinity:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.topologySpreadConstraints }}
      topologySpreadConstraints:
{{ toYaml . | indent 8 }}
    {{- end }}