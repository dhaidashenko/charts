# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2.0] - 2022-02-22

### Added

* Shorter resource labels for handy debugging.
* Option to inject Istio's sidecar.
* Option to extend default service environment variables.
* Expose additional service port for GRPC workloads.