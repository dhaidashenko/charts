{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "service.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "service.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "service.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels.

app.kubernetes.io labels domain seems to be recommended, we are using those for
selectors and stuff.

shorter aliases, e.g app and version, are easier to type in the console. should
not be used for other purposes.

*/}}
{{- define "service.labels" -}}
app.kubernetes.io/name: {{ include "service.name" . }}
helm.sh/chart: {{ include "service.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/version: {{ .Values.image.version | quote }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app: {{ .Release.Name }}
version: {{ .Values.image.version | quote }}
{{- end -}}
